package service

import (
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/routers/api/req"
)

// If HandleOauthLogin create new user it returns (user, true, nil) otherwise (user, false, nil)
func (s *Service) HandleOauthLogin(r *req.PostUserBody) (*model.User, bool, error) {
	usrMdl := &model.User{
		FirstName: r.FirstName,
		LastName:  r.LastName,
		Email:     r.Email,
	}
	err := usrMdl.GetByEmail()
	if err != nil {
		if model.IsNotFoundError(err) {
			usrMdl.LoginType = r.LoginType
			if err := usrMdl.Create(); err != nil {
				return nil, false, err
			}
			return usrMdl, true, nil
		}
	}
	return usrMdl, false, nil
}
