package service

import "errors"

// NotFoundError holds the name of the resource that is not found
type NotFoundError struct {
	name string
}

func (e NotFoundError) Error() string {
	return "service: " + e.name + " not found"
}

var ErrEmailAlreadyExists = errors.New("email already exists")
var ErrInvalidPassword = errors.New("invalid password")
