package service

import "bitbucket.org/sanjid09/sa_ho/config"

type Service struct {
	smtpInfo     config.SMTP
	passResetUrl string
}

func NewService(smtp config.SMTP, purl string) *Service {
	return &Service{
		smtpInfo:     smtp,
		passResetUrl: purl,
	}
}
