package service

import (
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/routers/api/req"
	"bitbucket.org/sanjid09/sa_ho/util"
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"golang.org/x/crypto/pbkdf2"
	"time"
)

// CreateUser Register user, store user into database
func (s *Service) CreateUser(usr *req.PostUserBody) (*model.User, error) {
	usrMdl := &model.User{
		FirstName: usr.FirstName,
		LastName:  usr.LastName,
		Email:     usr.Email,
	}
	err := usrMdl.GetByEmail()
	if err == nil {
		return nil, ErrEmailAlreadyExists
	} else {
		if !model.IsNotFoundError(err) {
			return nil, err
		}
	}
	usrMdl.LoginType = usr.LoginType

	// Hash password
	if err := SetPassword(usrMdl, usr.Password); err != nil {
		return nil, err
	}
	// Store into database
	if err := usrMdl.Create(); err != nil {
		return nil, err
	}
	return usrMdl, nil
}

// LoginUser get username, password and return user if valid, otherwise return error
func (s *Service) LoginUser(login *req.LoginBody) (*model.User, error) {
	usr := &model.User{
		Email: login.Email,
	}
	if err := usr.GetByEmail(); err != nil {
		return nil, err
	}
	if usr.LoginType != model.LoginPlain && login.Password == "" {
		return nil, ErrInvalidPassword
	}
	if ok, err := VerifyPassword(usr, login.Password); !ok {
		return nil, err
	}
	return usr, nil
}

// GetUser receives user_id and retrun user
func (s *Service) GetUser(uid uint) (*model.User, error) {
	user, err := model.GetUserByID(uid)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// UpdateUser update user value
func (s *Service) UpdateUser(uid uint, r *req.UpdateUserBody) error {
	user, err := model.GetUserByID(uid)
	if err != nil {
		return err
	}
	if r.Email != "" && user.Email != r.Email {
		ue := &model.User{Email: r.Email}
		if err := ue.GetByEmail(); err == nil {
			return ErrEmailAlreadyExists
		} else {
			if !model.IsNotFoundError(err) {
				return err
			}
		}
		user.Email = r.Email
	}
	user.FirstName = r.FirstName
	user.LastName = r.LastName
	user.Address = r.Address
	user.Telephone = r.Telephone
	if err := user.Update(); err != nil {
		return err
	}
	return nil
}

/* ForgetPassword works following
1. Check if user exists for provided email
2. Generate a token
3. Store token into database
4. Send the token to user email with a reset link
*/
func (s *Service) ForgetPassword(r *req.ForgetPasswordBody) error {
	usr := &model.User{
		Email: r.Email,
	}
	err := usr.GetByEmail()
	if err != nil {
		return err
	}
	token := util.RandomCharacters(20)
	pr := &model.PasswordRecover{
		UserID:   usr.ID,
		Email:    usr.Email,
		Token:    token,
		ExpireAt: time.Now().Add(6 * time.Hour),
	}
	if err := pr.Create(); err != nil {
		return err
	}
	email, err := getPasswordResetEmail(s.passResetUrl, usr.Email, token)
	if err != nil {
		return err
	}
	return s.SendEmail(usr.Email, email)
}

/*
	ResetPassword firs validate provided reset token against email, if valid then reset password
*/
func (s *Service) ResetPassword(r *req.ResetPasswordBody) (*model.User, error) {
	// check if valid token provided or not
	pr, err := model.GetPassRecovery(r.Email, r.Token)
	if err != nil {
		return nil, err
	}
	usr := &model.User{
		Email: pr.Email,
	}
	err = usr.GetByEmail()
	if err != nil {
		return nil, err
	}

	if err := SetPassword(usr, r.Password); err != nil {
		return nil, err
	}
	if err := usr.Update(); err != nil {
		return nil, err
	}
	return usr, nil
}

// SetPassword sets password fields for user u
func SetPassword(u *model.User, pass string) error {
	u.PassSalt = make([]byte, 16)
	if _, err := rand.Read(u.PassSalt); err != nil {
		return err
	}
	u.PassAlgo = model.AlgoSha256
	u.PassIter = 10000
	u.PassHash = pbkdf2.Key([]byte(pass), u.PassSalt, u.PassIter, 32, sha256.New)
	return nil
}

// VerifyPassword verifies if pass matcher to user u
func VerifyPassword(u *model.User, pass string) (bool, error) {
	hash := pbkdf2.Key([]byte(pass), u.PassSalt, u.PassIter, 32, sha256.New)
	if !bytes.Equal(u.PassHash, hash) {
		return false, ErrInvalidPassword
	}
	return true, nil
}
