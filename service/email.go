package service

import (
	"bitbucket.org/sanjid09/sa_ho/web/assets"
	"fmt"
	"html/template"
	"net/smtp"
)

var resetTpl *template.Template

func init() {
	htmlData := assets.MustAssetString("web/templates/password_reset_mail.html")
	resetTpl = template.Must(template.New("reset_view").Parse(htmlData))

}
func (s *Service) SendEmail(email string, data string) error {
	to := []string{
		email,
	}
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "Click here to reset\n"
	message := []byte(mime + subject + data)
	// Authentication.
	address := fmt.Sprintf("%s:%d", s.smtpInfo.Host, s.smtpInfo.Port)
	auth := smtp.PlainAuth("", s.smtpInfo.Username, s.smtpInfo.Password, s.smtpInfo.Host)
	// Sending email.
	err := smtp.SendMail(address, auth, "info@saho.com", to, message)
	if err != nil {
		return err
	}
	return nil
}

// Parse template with email and token
func getPasswordResetEmail(addr, email, token string) (string, error) {
	url := fmt.Sprintf("%s?email=%s&token=%s", addr, email, token)
	return url, nil
	/*
		buff := new(bytes.Buffer)
		data := map[string]interface{}{
			"Link": url,
		}
		if err := resetTpl.ExecuteTemplate(buff, "reset_view", data); err != nil {
			return "", err
		}
		return buff.String(), nil */
}
