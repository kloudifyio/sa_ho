package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	x      *gorm.DB
	tables []interface{}
)

func init() {
	tables = append(tables,
		new(User),
		new(PasswordRecover),
	)
}

func migrations() error {
	return x.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(tables...).Error
}

// SetMysqlStore initialize mysql db
func SetMysqlStore(host, name, username, password string) error {
	sql := fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local", username, password, host, name)
	db, err := gorm.Open("mysql", sql)
	if err != nil {
		return err
	}
	db.LogMode(true) // Log enable for debugging purpose
	x = db

	return migrations()

}
