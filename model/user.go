package model

import (
	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
)

type LoginType int

const (
	LoginNoType LoginType = iota
	LoginPlain            // 1
	LoginOAuth2           // 6
)
const (
	AlgoSha256 = "sha256"
)

type User struct {
	gorm.Model
	Email     string `gorm:"unique; not null"`
	FirstName string
	LastName  string
	Address   string
	Telephone string

	PassHash []byte
	PassSalt []byte
	PassIter int
	PassAlgo string

	LoginType LoginType
}

// Create store user into database
func (u *User) Create() error {
	if ok, err := u.Valid(); !ok {
		return err
	}
	if err := x.Create(u).Error; err != nil {
		return err
	}
	return nil
}

// Update update user information
func (u *User) Update() error {
	return x.Save(u).Error
}

// GetByEmail Returns user match with email
func (u *User) GetByEmail() error {
	if err := x.Where("email = ?", u.Email).First(u).Error; err != nil {
		return err
	}
	return nil
}

// GetUserByID returns user match with ID
func GetUserByID(uid uint) (*User, error) {
	u := &User{}
	if err := x.Where("id = ?", uid).First(u).Error; err != nil {
		return nil, err
	}
	return u, nil
}

// Valid checks if the user data is valid to store
func (u *User) Valid() (bool, error) {
	errs := ValidationError{}
	if u.Email == "" {
		errs.Add("email", "is required")
	}
	if u.Email != "" && !govalidator.IsEmail(u.Email) {
		errs.Add("Email", "is invalid")
	}

	if len(u.PassHash) > 0 {
		if u.PassAlgo == "" {
			errs.Add("PassAlgo", "is required")
		}
		if u.PassIter < 1 {
			errs.Add("PassIter", "is invalid")
		}
		if len(u.PassSalt) == 0 {
			errs.Add("PassSalt", "is required")
		}
	}

	if len(errs) > 0 {
		return false, errs
	}

	return true, nil
}
