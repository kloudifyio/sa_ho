package model

import (
	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"time"
)

type PasswordRecover struct {
	gorm.Model
	UserID   uint
	Email    string `gorm:"primary_key;auto_increment:false"`
	Token    string `gorm:"primary_key;auto_increment:false"`
	ExpireAt time.Time
}

// Create store token on database
func (p *PasswordRecover) Create() error {
	if ok, err := p.Valid(); !ok {
		return err
	}
	if err := x.Create(p).Error; err != nil {
		return err
	}
	return nil
}

// GetPassRecovery returns PasswordRecover structs if email and token found on database
func GetPassRecovery(email, token string) (*PasswordRecover, error) {
	p := &PasswordRecover{}
	if err := x.Where("email = ? AND token = ? AND expire_at >= ?", email, token, time.Now()).First(p).Error; err != nil {
		return nil, err
	}
	return p, nil
}

// Valid checks if the password recovery data is valid to store
func (p *PasswordRecover) Valid() (bool, error) {
	errs := ValidationError{}
	if p.Email == "" {
		errs.Add("email", "is required")
	}
	if p.Email != "" && !govalidator.IsEmail(p.Email) {
		errs.Add("Email", "is invalid")
	}
	if p.Token == "" {
		errs.Add("token", "is required")
	}
	if p.ExpireAt.IsZero() {
		errs.Add("expire_at", "is required")
	}
	if len(errs) > 0 {
		return false, errs
	}

	return true, nil
}
