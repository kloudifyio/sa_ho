package model

import (
	"encoding/json"
	"github.com/jinzhu/gorm"
)

// ValidationError holds the validation errors of model
type ValidationError map[string][]string

func (e ValidationError) Error() string {
	data, _ := json.Marshal(e)
	return "model: invalid data" + string(data)
}

// Add adds validation msg of the field
func (e ValidationError) Add(key, msg string) {
	e[key] = append(e[key], msg)
}

func IsNotFoundError(err error) bool {
	if err == gorm.ErrRecordNotFound {
		return true
	}
	return false
}
