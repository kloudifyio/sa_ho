package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

// Application holds application configurations
type Application struct {
	Host             string       `yaml:"host"`
	Port             int          `yaml:"port"`
	MySql            MySql        `yaml:"mysql"`
	Oauth            *OauthConfig `yaml:"oauth"`
	SMTP             SMTP         `yaml:"smtp"`
	ResetPasswordURL string       `yaml:"reset_password_url"`
	GracefulTimeout  int          `yaml:"graceful_timeout"`
}

type SMTP struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

type MySql struct {
	Host     string `yaml:"host"`
	Database string `yaml:"database"`
	UserName string `yaml:"username"`
	Password string `yaml:"password"`
}

type OauthConfig struct {
	Google ProviderSecret `yaml:"google"`
}

type ProviderSecret struct {
	ClientID     string `yaml:"clientID"`
	ClientSecret string `yaml:"clientSecret"`
	RedirectURL  string `yaml:"redirectURL"`
}

// Load loads config from path
func Load(path string, cfg interface{}) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := yaml.NewDecoder(f).Decode(cfg); err != nil {
		return err
	}
	return nil
}
