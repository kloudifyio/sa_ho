package main

import (
	"bitbucket.org/sanjid09/sa_ho/config"
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/routers/api"
	"bitbucket.org/sanjid09/sa_ho/routers/web"
	"bitbucket.org/sanjid09/sa_ho/service"
	"context"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
)

// srvCmd is the serve sub command to start the api server
var srvCmd = &cobra.Command{
	Use:   "serve",
	Short: "serve serves the api server",
	RunE:  serve,
}

func init() {
	srvCmd.PersistentFlags().StringVarP(&cfgPath, "config", "c", "", "config file path")
}

func serve(cmd *cobra.Command, args []string) error {
	cfg := config.Application{}
	err := config.Load(cfgPath, &cfg)
	if err != nil {
		return err
	}
	// Initialize database
	mysql := cfg.MySql
	err = model.SetMysqlStore(mysql.Host, mysql.Database, mysql.UserName, mysql.Password)
	if err != nil {
		return err
	}
	svc := service.NewService(cfg.SMTP, cfg.ResetPasswordURL)
	r := mux.NewRouter()
	pc := proficerOauthConfig(cfg.Oauth)
	apiServer := api.InitializeApiServerRouter(svc)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	r.HandleFunc("/", web.IndexRoute)
	mount(r, "/web/", web.WebRouters(pc))
	mount(r, "/api/", apiServer.ApiRouters())

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	addr := fmt.Sprintf("%s:%d", cfg.Host, cfg.Port)
	srv := &http.Server{
		Addr:         addr,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		Handler:      handlers.CORS(originsOk, headersOk, methodsOk)(r),
	}

	go func() {
		log.Println("Staring server with address ", addr)
		if err := srv.ListenAndServe(); err != nil {
			log.Println("Failed to listen and serve", err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c
	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(cfg.GracefulTimeout)*time.Second)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)

	return nil
}

func proficerOauthConfig(conf *config.OauthConfig) map[string]*oauth2.Config {
	pc := make(map[string]*oauth2.Config)
	gconf := &oauth2.Config{
		ClientID:     conf.Google.ClientID,
		ClientSecret: conf.Google.ClientSecret,
		RedirectURL:  conf.Google.RedirectURL,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}
	pc["google"] = gconf
	return pc

}

func mount(r *mux.Router, path string, handler http.Handler) {
	r.PathPrefix(path).Handler(
		http.StripPrefix(
			strings.TrimSuffix(path, "/"),
			handler,
		),
	)
}
