package main

import (
	"bitbucket.org/sanjid09/sa_ho/version"
	"github.com/spf13/cobra"
)

var cfgPath string

// rootCmd is the root of all sub commands in the binary
// it doesn't have a Run method as it executes other sub commands
var rootCmd = &cobra.Command{
	Use:     "sa_ho",
	Short:   "sa_ho is a http server to serve api",
	Version: version.Version,
}

func init() {
	// Here all other sub commands should be registered to the rootCmd
	rootCmd.AddCommand(srvCmd)
}

func main() {
	rootCmd.Execute()
}
