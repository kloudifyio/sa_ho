### How to build locally

```bash
./build.sh
```

### How to deploy

```bash
cp config.example.yaml config.yaml
# update config.yaml with appropriate value, then
docker-compse up --build
```