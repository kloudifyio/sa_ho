package util

import "math/rand"

var randChars = []rune("abcdefghijklmnopqrstuvwxyz234567")

// Use this for generating random pat of a ID. Do not use this for generating short passwords or secrets.
func RandomCharacters(len int) string {
	bytes := make([]byte, len)
	rand.Read(bytes)
	r := make([]rune, len)
	for i, b := range bytes {
		r[i] = randChars[b>>3]
	}
	return string(r)
}

func WithUniqSuffix(seed string) string {
	return seed + "-" + RandomCharacters(6)
}
