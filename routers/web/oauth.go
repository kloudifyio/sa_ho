package web

import (
	"bitbucket.org/sanjid09/sa_ho/routers/api/req"
	"bitbucket.org/sanjid09/sa_ho/routers/api/resp"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dchest/uniuri"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"net/http"
)

type Oauth struct {
	conf map[string]*oauth2.Config
}

func (o *Oauth) loginProviderHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	provider := vars["provider"]
	cnf := o.conf[provider]
	oauthStateString := uniuri.New()
	url := cnf.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (o *Oauth) loginProviderCallback(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")
	vars := mux.Vars(r)
	provider := vars["provider"]
	cnf := o.conf[provider]

	token, err := cnf.Exchange(oauth2.NoContext, code)
	if err != nil {
		resp.ServeUnauthorized(w, r, err)
		return
	}

	if !token.Valid() {
		resp.ServeUnauthorized(w, r, errors.New("Retreived invalid token"))
		return
	}
	body, err := json.Marshal(&req.OauthBody{
		AccessToken: token.AccessToken,
	})
	if err != nil {
		resp.ServeError(w, r, err)
	}
	res, err := makeApiCall(r, "POST", fmt.Sprintf("/api/signin/%s", provider), string(body), false)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	u, _ := json.Marshal(res.Data)
	respUser := &resp.OAuth{}
	json.Unmarshal(u, respUser)

	http.SetCookie(w, &http.Cookie{
		Name:  "Authorization",
		Value: fmt.Sprintf("Bearer %s", respUser.Token),
		Path:  "/",
	})
	url := "/web/profile"
	if respUser.IsSignup {
		url = "/web/profile/edit"
	}
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
	return
}
