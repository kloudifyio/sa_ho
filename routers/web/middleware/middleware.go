package middleware

import (
	"bitbucket.org/sanjid09/sa_ho/routers/api/auth"
	"net/http"
)

func WithAuth(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := auth.Valid(r)
		if err != nil {
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		next(w, r)
	}
}
