package web

import (
	"bitbucket.org/sanjid09/sa_ho/routers/api/resp"
	"encoding/json"
	"html/template"
	"net/http"
)

func ProfileEditRoute(w http.ResponseWriter, r *http.Request) {
	res, err := makeApiCall(r, "GET", "/api/user/me", "", true)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	u, _ := json.Marshal(res.Data)
	respUser := &resp.User{}
	json.Unmarshal(u, respUser)
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarLoginHTML),
		"FirstName":     respUser.FirstName,
		"LastName":      respUser.LastName,
		"Email":         respUser.Email,
		"Address":       respUser.Address,
		"Telephone":     respUser.Telephone,
	}
	render(w, r, templates["profile-edit"], "profile_edit_view", fullData)

}

func ProfileRoute(w http.ResponseWriter, r *http.Request) {
	res, err := makeApiCall(r, "GET", "/api/user/me", "", true)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	u, _ := json.Marshal(res.Data)
	respUser := &resp.User{}
	json.Unmarshal(u, respUser)
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarLoginHTML),
		"Name":          respUser.FirstName + " " + respUser.LastName,
		"Email":         respUser.Email,
		"Address":       respUser.Address,
		"Telephone":     respUser.Telephone,
	}
	render(w, r, templates["profile"], "profile_view", fullData)
}

func IndexRoute(w http.ResponseWriter, r *http.Request) {
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarHTML),
	}

	render(w, r, templates["index"], "homepage_view", fullData)

}

func SigninRoute(w http.ResponseWriter, r *http.Request) {
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarHTML),
	}
	render(w, r, templates["signin"], "signin_view", fullData)

}

func SignupRoute(w http.ResponseWriter, r *http.Request) {
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarHTML),
	}
	render(w, r, templates["signup"], "signup_view", fullData)
}

func ForgetPasswordRoute(w http.ResponseWriter, r *http.Request) {
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarHTML),
	}
	render(w, r, templates["forget-password"], "forget_password_view", fullData)
}

func ResetPasswordRoute(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	token := r.URL.Query().Get("token")
	fullData := map[string]interface{}{
		"NavigationBar": template.HTML(navigationBarHTML),
		"Email":         email,
		"Token":         token,
	}
	render(w, r, templates["reset-password"], "reset_password_view", fullData)
}
