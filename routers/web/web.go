package web

import (
	"bitbucket.org/sanjid09/sa_ho/routers/api/resp"
	"bitbucket.org/sanjid09/sa_ho/routers/web/middleware"
	"bitbucket.org/sanjid09/sa_ho/web/assets"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

var templates map[string]*template.Template
var navigationBarHTML string
var navigationBarLoginHTML string
var ApiServerAddress string

func init() {
	loadTemplates()
	if addr := os.Getenv("API_SERVER_ADDRESS"); addr != "" {
		ApiServerAddress = addr
	}
}

func WebRouters(providerConf map[string]*oauth2.Config) *mux.Router {
	r := mux.NewRouter()
	o := &Oauth{
		conf: providerConf,
	}
	r.HandleFunc("/", IndexRoute).Methods("GET")
	r.HandleFunc("/signin", SigninRoute).Methods("GET")
	r.HandleFunc("/signin/{provider}/connect", o.loginProviderHandler).Methods("GET")
	r.HandleFunc("/signin/{provider}/callback", o.loginProviderCallback).Methods("GET")
	r.HandleFunc("/forget-password", ForgetPasswordRoute).Methods("GET")
	r.HandleFunc("/reset-password", ResetPasswordRoute).Methods("GET")
	r.HandleFunc("/signup", SignupRoute).Methods("GET")
	r.HandleFunc("/profile/edit", middleware.WithAuth(ProfileEditRoute)).Methods("GET")
	r.HandleFunc("/profile", middleware.WithAuth(ProfileRoute)).Methods("GET")
	return r
}

func makeApiCall(r *http.Request, method, url, body string, authicated bool) (*resp.JSONResponse, error) {
	url = fmt.Sprintf("%s%s", ApiServerAddress, url)
	var reqData io.Reader
	if body != "" {
		reqData = bytes.NewBuffer([]byte(body))
	} else {
		reqData = nil
	}
	req, err := http.NewRequest(method, url, reqData)
	if err != nil {
		return nil, err
	}
	switch method {
	case "POST":
		req.Header.Set("Content-type", "application/json")

	}
	if authicated {
		ck, err := r.Cookie("Authorization")
		if err != nil {
			return nil, err
		}
		if ck != nil {
			req.Header.Set("Authorization", ck.Value)
		}
	}
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	jsonResp := &resp.JSONResponse{}
	if err := json.Unmarshal(data, jsonResp); err != nil {
		return nil, err
	}
	return jsonResp, nil
}

func loadTemplates() {
	navigationBarHTML = assets.MustAssetString("web/templates/navigation_bar.html")
	navigationBarLoginHTML = assets.MustAssetString("web/templates/profile_nav_bar.html")

	indexData := assets.MustAssetString("web/templates/index.html")
	indexTpl := template.Must(template.New("homepage_view").Parse(indexData))
	templates = make(map[string]*template.Template)
	templates["index"] = indexTpl
	templates["signup"] = template.Must(template.New("signup_view").Parse(assets.MustAssetString("web/templates/signup.html")))
	templates["signin"] = template.Must(template.New("signin_view").Parse(assets.MustAssetString("web/templates/signin.html")))
	templates["profile"] = template.Must(template.New("profile_view").Parse(assets.MustAssetString("web/templates/profile.html")))
	templates["profile-edit"] = template.Must(template.New("profile_edit_view").Parse(assets.MustAssetString("web/templates/profile_edit.html")))
	templates["forget-password"] = template.Must(template.New("forget_password_view").Parse(assets.MustAssetString("web/templates/forget-pass.html")))
	templates["reset-password"] = template.Must(template.New("reset_password_view").Parse(assets.MustAssetString("web/templates/password_reset.html")))
}

// Render a template, or server error.
func render(w http.ResponseWriter, r *http.Request, tpl *template.Template, name string, data interface{}) {
	buf := new(bytes.Buffer)
	if err := tpl.ExecuteTemplate(buf, name, data); err != nil {
		fmt.Printf("\nRender Error: %v\n", err)
		return
	}
	w.Write(buf.Bytes())
}

// Push the given resource to the client.
func push(w http.ResponseWriter, resource string) {
	pusher, ok := w.(http.Pusher)
	if ok {
		if err := pusher.Push(resource, nil); err == nil {
			return
		}
	}
}
