package auth

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type Auth struct {
	Token    string
	LoginAt  time.Time
	ExpireAt time.Time
}

func GenerateToken(uid uint) (*Auth, error) {
	claims := jwt.MapClaims{}
	now := time.Now()
	exp := now.Add(time.Hour * 24) //Token expires after 6 hour
	claims["authorized"] = true
	claims["user_id"] = uid
	claims["exp"] = exp
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tknSign, err := token.SignedString([]byte(os.Getenv("API_SECRET")))
	if err != nil {
		return nil, err
	}
	return &Auth{
		Token:    tknSign,
		LoginAt:  now,
		ExpireAt: exp,
	}, nil
}

func Valid(r *http.Request) error {
	tokenString := GetToken(r)
	if tokenString == "" {
		return errors.New("Unauthorized")
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("API_SECRET")), nil
	})
	if err != nil {
		return err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		r.Header.Set("uid", fmt.Sprintf("%.0f", claims["user_id"]))
	}
	return nil
}

func GetToken(r *http.Request) string {
	bearerToken := r.Header.Get("Authorization")
	if bearerToken == "" {
		// If not found in header then check for cookie
		ck, _ := r.Cookie("Authorization")
		if ck != nil {
			bearerToken = ck.Value
		}
	}
	if !strings.HasPrefix(bearerToken, "Bearer ") {
		return ""
	}
	bearerToken = strings.TrimSpace(strings.TrimPrefix(bearerToken, "Bearer "))
	return bearerToken
}

func GetUserID(r *http.Request) (uint, error) {
	tokenString := GetToken(r)

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("API_SECRET")), nil
	})
	if err != nil {
		return 0, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		uid, err := strconv.ParseUint(fmt.Sprintf("%.0f", claims["user_id"]), 10, 32)
		if err != nil {
			return 0, err
		}
		return uint(uid), nil
	}
	return 0, nil
}
