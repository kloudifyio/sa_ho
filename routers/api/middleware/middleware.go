package middleware

import (
	"bitbucket.org/sanjid09/sa_ho/routers/api/auth"
	"bitbucket.org/sanjid09/sa_ho/routers/api/resp"
	"net/http"
)

func WithAuth(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := auth.Valid(r)
		if err != nil {
			resp.ServeUnauthorized(w, r, err)
			return
		}
		next(w, r)
	}
}
