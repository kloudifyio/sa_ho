package api

import (
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/routers/api/auth"
	"bitbucket.org/sanjid09/sa_ho/routers/api/req"
	"bitbucket.org/sanjid09/sa_ho/routers/api/resp"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/asaskevich/govalidator"
	"net/http"
)

// CreateUserHandler Register User
func (s *Server) CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	usr := &req.PostUserBody{}
	err := json.NewDecoder(r.Body).Decode(usr)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if _, err := govalidator.ValidateStruct(usr); err != nil {
		dtls := processValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return

	}
	usr.LoginType = model.LoginPlain
	lu, err := s.svc.CreateUser(usr)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}
	respAuth, err := s.loginUser(lu, w)
	if err != nil {
		resp.ServeError(w, r, err)
	}
	resp.ServeData(w, r, http.StatusOK, respAuth, nil)
	return
}

func (s *Server) LoginUserHandler(w http.ResponseWriter, r *http.Request) {
	loginBody := &req.LoginBody{}
	err := json.NewDecoder(r.Body).Decode(loginBody)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if _, err := govalidator.ValidateStruct(loginBody); err != nil {
		dtls := processValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return

	}
	usr, err := s.svc.LoginUser(loginBody)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}

	lu, err := s.loginUser(usr, w)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}
	resp.ServeData(w, r, http.StatusOK, lu, nil)
	return
}

func (s *Server) ForgetPassword(w http.ResponseWriter, r *http.Request) {
	passBody := &req.ForgetPasswordBody{}
	err := json.NewDecoder(r.Body).Decode(passBody)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if _, err := govalidator.ValidateStruct(passBody); err != nil {
		dtls := processValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return

	}
	if err := s.svc.ForgetPassword(passBody); err != nil {
		resp.ServeError(w, r, err)
		return
	}
	resp.ServeData(w, r, http.StatusOK, "ok", nil)
	return
}

// ResetPassword reset password
func (s *Server) ResetPassword(w http.ResponseWriter, r *http.Request) {
	passBody := &req.ResetPasswordBody{}
	err := json.NewDecoder(r.Body).Decode(passBody)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if _, err := govalidator.ValidateStruct(passBody); err != nil {
		dtls := processValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return

	}
	usr, err := s.svc.ResetPassword(passBody)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}
	lu, err := s.loginUser(usr, w)
	if err != nil {
		resp.ServeError(w, r, err)
	}
	resp.ServeData(w, r, http.StatusOK, lu, nil)
	return
}

func (s *Server) GetMeHandler(w http.ResponseWriter, r *http.Request) {
	uid, err := auth.GetUserID(r)
	if err != nil {
		resp.ServeUnauthorized(w, r, err)
		return
	}
	if uid == 0 {
		resp.ServeUnauthorized(w, r, errors.New("User not found"))
		return
	}
	usr, err := s.svc.GetUser(uid)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}
	resp.ServeData(w, r, http.StatusOK, resp.PrepareUser(usr), nil)
	return
}

// UpdateHandler update user
func (s *Server) UpdateHandler(w http.ResponseWriter, r *http.Request) {
	uid, err := auth.GetUserID(r)
	if err != nil {
		resp.ServeUnauthorized(w, r, err)
		return
	}
	if uid == 0 {
		resp.ServeUnauthorized(w, r, errors.New("User not found"))
		return
	}
	updateBody := &req.UpdateUserBody{}
	err = json.NewDecoder(r.Body).Decode(updateBody)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if _, err := govalidator.ValidateStruct(updateBody); err != nil {
		dtls := processValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return

	}
	if err := s.svc.UpdateUser(uid, updateBody); err != nil {
		resp.ServeError(w, r, err)
		return
	}
	resp.ServeData(w, r, http.StatusOK, "OK", nil)
	return
}

// LogoutHandler clear Authorization cookie
func (s *Server) LogoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:  "Authorization",
		Value: "",
		Path:  "/",
	})
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	return
}

// loginUser generate a bearer token for user and store it into cookie
func (s *Server) loginUser(usr *model.User, w http.ResponseWriter) (*resp.Auth, error) {
	a, err := auth.GenerateToken(usr.ID)
	if err != nil {
		return nil, err
	}

	http.SetCookie(w, &http.Cookie{
		Name:  "Authorization",
		Value: fmt.Sprintf("Bearer %s", a.Token),
		Path:  "/",
	})
	return resp.PrepareAuth(usr, a), nil

}
