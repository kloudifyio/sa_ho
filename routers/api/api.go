package api

import (
	"bitbucket.org/sanjid09/sa_ho/routers/api/middleware"
	"bitbucket.org/sanjid09/sa_ho/service"
	"errors"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
)

var (
	ErrInvalidData = errors.New("Invalid data")
)

type Server struct {
	svc *service.Service
}

func InitializeApiServerRouter(svc *service.Service) *Server {
	s := &Server{
		svc: svc,
	}
	return s
}

func (s *Server) ApiRouters() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/register", s.CreateUserHandler).Methods("POST")
	r.HandleFunc("/login", s.LoginUserHandler).Methods("POST")
	r.HandleFunc("/signin/{provider}", s.Oauth2LoginHandler).Methods("POST")
	r.HandleFunc("/password/forget", s.ForgetPassword).Methods("POST")
	r.HandleFunc("/password/reset", s.ResetPassword).Methods("POST")
	r.HandleFunc("/user/me", middleware.WithAuth(s.GetMeHandler)).Methods("GET")
	r.HandleFunc("/user", middleware.WithAuth(s.UpdateHandler)).Methods("PUT")
	r.HandleFunc("/logout", middleware.WithAuth(s.LogoutHandler)).Methods("GET")
	return r
}

func processValidationErr(err error) map[string]interface{} {
	msgs := govalidator.ErrorsByField(err)
	dtls := map[string]interface{}{}
	for k, v := range msgs {
		dtls[k] = v
	}
	return dtls
}
