package resp

import (
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/routers/api/auth"
	"time"
)

// User presents the response object of a user
type User struct {
	ID        uint      `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Email     string    `json:"email"`
	Address   string    `json:"address"`
	Telephone string    `json:"telephone"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

// Auth presents the response object of login
type Auth struct {
	Token      string    `json:"token"`
	User       *User     `json:"user"`
	LoggedInAt time.Time `json:"loggedInAt"`
	ExpiresAt  time.Time `json:"expiresAt"`
}

type OAuth struct {
	Token    string `json:"token"`
	IsSignup bool   `json:"is_signup"`
}

func PrepareAuth(u *model.User, a *auth.Auth) *Auth {
	return &Auth{
		Token:      a.Token,
		User:       PrepareUser(u),
		LoggedInAt: a.LoginAt,
		ExpiresAt:  a.ExpireAt,
	}
}

func PrepareUser(u *model.User) *User {
	return &User{
		ID:        u.ID,
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
		Address:   u.Address,
		Telephone: u.Telephone,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}
}
