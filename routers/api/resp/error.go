package resp

import (
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/service"
	"bitbucket.org/sanjid09/sa_ho/util"
	"net/http"
)

// Error represents a response object of api error
type Error struct {
	ID         string                 `json:"id,omitempty"`
	Message    string                 `json:"message,omitempty"`
	Details    map[string]interface{} `json:"details,omitempty"`
	StackTrace string                 `json:"stackTrace,omitempty"`
}

// ServeBadRequest serves http BadRequest
func ServeBadRequest(w http.ResponseWriter, r *http.Request, err error) {
	re := &JSONResponse{
		response: response{code: http.StatusBadRequest},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
			},
		},
	}
	Render(w, r, re)
}

// ServeNotFound serves http NotFound
func ServeNotFound(w http.ResponseWriter, r *http.Request, err error) {
	re := &JSONResponse{
		response: response{code: http.StatusNotFound},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
			},
		},
	}
	Render(w, r, re)
}

// ServeUnauthorized serves http Unauthorized
func ServeUnauthorized(w http.ResponseWriter, r *http.Request, err error) {
	re := &JSONResponse{
		response: response{code: http.StatusUnauthorized},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
			},
		},
	}
	Render(w, r, re)
}

// ServeForbidden serves http Forbidden
func ServeForbidden(w http.ResponseWriter, r *http.Request, err error) {
	re := &JSONResponse{
		response: response{code: http.StatusForbidden},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
			},
		},
	}
	Render(w, r, re)
}

// ServeUnprocessableEntity serves http UnprocessableEntity
func ServeUnprocessableEntity(w http.ResponseWriter, r *http.Request, err error, dtl map[string]interface{}) {
	re := &JSONResponse{
		response: response{code: http.StatusUnprocessableEntity},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
				Details: dtl,
			},
		},
	}
	Render(w, r, re)
}

// ServeInternalServerError serves http InternalServerError
func ServeInternalServerError(w http.ResponseWriter, r *http.Request, err error) {
	re := &JSONResponse{
		response: response{code: http.StatusInternalServerError},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
			},
		},
	}
	Render(w, r, re)
}

// ServeTooManyRequestsError serves http TooManyRequests
func ServeTooManyRequestsError(w http.ResponseWriter, r *http.Request, err error) {
	re := &JSONResponse{
		response: response{code: http.StatusTooManyRequests},
		Errors: []Error{
			{
				ID:      util.RandomCharacters(10),
				Message: err.Error(),
			},
		},
	}
	Render(w, r, re)
}

// ServeError serves error with appropriate http status code determined from error type
func ServeError(w http.ResponseWriter, r *http.Request, err error) {
	switch err := err.(type) {
	case model.ValidationError:
		dtl := map[string]interface{}{}
		for k, v := range err {
			dtl[k] = v
		}
		ServeUnprocessableEntity(w, r, err, dtl)
	case service.NotFoundError:
		ServeNotFound(w, r, err)
	default:
		ServeInternalServerError(w, r, err)
	}
}
