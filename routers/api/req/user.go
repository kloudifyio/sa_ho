package req

import "bitbucket.org/sanjid09/sa_ho/model"

type PostUserBody struct {
	Email     string          `json:"email" valid:"required"`
	FirstName string          `json:"firstName" valid:"-"`
	LastName  string          `json:"lastName" valid:"-"`
	Password  string          `json:"password" valid:"required"`
	LoginType model.LoginType `json:"login_type" valid:"-"`
}

type LoginBody struct {
	Email    string `json:"email" valid:"required"`
	Password string `json:"password" valid:"required"`
}

type UpdateUserBody struct {
	Email     string `json:"email" valid:"required"`
	FirstName string `json:"firstName" valid:"-"`
	LastName  string `json:"lastName" valid:"-"`
	Address   string `json:"address" valid:"-"`
	Telephone string `json:"telephone" valid:"-"`
}

type ForgetPasswordBody struct {
	Email string `json:"email" valid:"required"`
}

type ResetPasswordBody struct {
	Email    string `json:"email" valid:"required"`
	Token    string `json:"token" valid:"required"`
	Password string `json:"password" valid:"required"`
}

type OauthBody struct {
	AccessToken string `json:"access_token" valid:"required"`
}
