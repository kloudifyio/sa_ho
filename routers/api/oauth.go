package api

import (
	"bitbucket.org/sanjid09/sa_ho/model"
	"bitbucket.org/sanjid09/sa_ho/routers/api/auth"
	"bitbucket.org/sanjid09/sa_ho/routers/api/req"
	"bitbucket.org/sanjid09/sa_ho/routers/api/resp"
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
)

type GoogleUser struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Link          string `json:"link"`
	Picture       string `json:"picture"`
	Gender        string `json:"gender"`
	Locale        string `json:"locale"`
}

func (s *Server) Oauth2LoginHandler(w http.ResponseWriter, r *http.Request) {
	oauthBody := &req.OauthBody{}
	err := json.NewDecoder(r.Body).Decode(oauthBody)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if _, err := govalidator.ValidateStruct(oauthBody); err != nil {
		dtls := processValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return

	}

	vars := mux.Vars(r)
	provider := vars["provider"]
	rbody := &req.PostUserBody{}
	switch provider {
	case "google":
		response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + oauthBody.AccessToken)
		if err != nil {
			resp.ServeError(w, r, err)
			return
		}

		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			resp.ServeError(w, r, err)
			return
		}
		var user *GoogleUser
		err = json.Unmarshal(contents, &user)
		if err != nil {
			resp.ServeError(w, r, err)
			return
		}
		rbody.Email = user.Email
		rbody.FirstName = user.GivenName
		rbody.LastName = user.GivenName
		rbody.LoginType = model.LoginOAuth2
	}
	usr, isNew, err := s.svc.HandleOauthLogin(rbody)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}
	a, err := auth.GenerateToken(usr.ID)
	if err != nil {
		resp.ServeError(w, r, err)
		return
	}

	resp.ServeData(w, r, http.StatusOK, &resp.OAuth{
		Token:    a.Token,
		IsSignup: isNew,
	}, nil)
	return
}
